package co.lightbulb.instahorror;

public interface IconPagerAdapter {
	int getIconResId(int index);

	int getCount();
}