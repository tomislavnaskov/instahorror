package co.lightbulb.instahorror;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SlidingPaneLayout.LayoutParams;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public final class OverlaySlideFragment extends Fragment {
	int imageResourceId;

	public OverlaySlideFragment(int i) {
		imageResourceId = i;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		ImageView image = new ImageView(getActivity());
		image.setImageResource(imageResourceId);

		LinearLayout layout = new LinearLayout(getActivity());
		layout.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		layout.setGravity(Gravity.TOP);
		layout.addView(image);

		return layout;
	}
	
}
