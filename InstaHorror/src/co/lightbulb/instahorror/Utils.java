package co.lightbulb.instahorror;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.Images;

public class Utils {

	public static Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = Images.Media.insertImage(inContext.getContentResolver(),
				inImage, "Title", null);
		return Uri.parse(path);
	}

	public static Bitmap drawOverlay(Bitmap bmp1, Bitmap bmp2) {
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),
				bmp1.getHeight(), bmp1.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp1, new Matrix(), null);
		canvas.drawBitmap(bmp2, new Matrix(), null);
		return bmOverlay;
	}

	@SuppressLint("SimpleDateFormat")
	public static File getOutputMediaFile(Context context) {

		File ext = Environment.getExternalStorageDirectory();
		String exstPath = ext.getPath();

		File mediaStorageDir = new File(exstPath + "/"
				+ context.getString(R.string.folder_appname));

		if (!mediaStorageDir.exists()) {
			return null;
		}

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	public static boolean isPortrait(Context context) {
		return (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
	}

	public static Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 1.0;
		double targetRatio = (double) w / h;
		if (sizes == null)
			return null;

		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		for (Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}

	public static Camera.Size getBestPreviewSize(int width, int height,
			Camera camera) {
		Camera.Size result = null;
		Camera.Parameters p = camera.getParameters();
		for (Camera.Size size : p.getSupportedPreviewSizes()) {
			if ((size.width <= width && size.height <= height)
					|| (size.height <= width && size.width <= height)) {
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea > resultArea) {
						result = size;
					}
				}
			}
		}
		return result;
	}

	public static Size getOptimalPictureSize(int width, int height,
			Camera.Parameters parameters) {
		Camera.Size result = null;

		for (Camera.Size size : parameters.getSupportedPictureSizes()) {
			if (size.width <= width && size.height <= height) {
				if (result == null) {
					result = size;
				} else {
					int resultArea = result.width * result.height;
					int newArea = size.width * size.height;

					if (newArea > resultArea) {
						result = size;
					}
				}
			}
		}

		return (result);
	}

	public static void modifyAndSavePicture(byte[] data, Context context,
			int cameraId, File pictureFile) {
		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

		if (bitmap.getWidth() > bitmap.getHeight()) {
			Matrix matrix = new Matrix();
			if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				matrix.postRotate(270);
			} else {

				matrix.postRotate(90);
			}
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
					bitmap.getHeight(), matrix, true);
		}
		Bitmap overlay = BitmapFactory.decodeResource(
				context.getResources(),
				((Activity) context).getPreferences(0).getInt(
						C.SHARED_PREFS_OVERLAY_ITEM, R.drawable.instashot01));
		Matrix matrix = new Matrix();
		if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			matrix.preScale(-1.0f, 1.0f);
		} else {
			matrix.preScale(1.0f, 1.0f);
		}
		if (bitmap.getHeight() < overlay.getHeight()) {
			overlay = Bitmap.createScaledBitmap(overlay, bitmap.getHeight(),
					bitmap.getHeight(), true);
		}
		if (bitmap.getWidth() < overlay.getWidth()) {
			overlay = Bitmap.createScaledBitmap(overlay, bitmap.getWidth(),
					bitmap.getWidth(), true);
		}
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
				overlay.getWidth(), overlay.getHeight(), matrix, false);

		try {
			FileOutputStream fos = new FileOutputStream(pictureFile);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			Utils.drawOverlay(resizedBitmap, overlay).compress(
					CompressFormat.PNG, 0, bos);
			byte[] bitmapdata = bos.toByteArray();
			fos.write(bitmapdata);
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
