package co.lightbulb.instahorror;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

public class NativeCameraFragment extends Fragment {

	private Camera mCamera;

	private CameraPreview mPreview;

	private View mCameraView;

	private static int mCurrentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;

	public NativeCameraFragment() {
		super();
	}

	public static NativeCameraFragment newInstance(int sectionNumber) {
		NativeCameraFragment fragment = new NativeCameraFragment();
		Bundle args = new Bundle();
		args.putInt(C.ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_native_camera,
				container, false);

		boolean opened = safeCameraOpenInView(view);

		if (opened == false) {
			Log.d("CameraGuide", "Error, Camera failed to open");
			return view;
		}

		return view;
	}

	private boolean safeCameraOpenInView(View view) {
		boolean qOpened = false;
		releaseCameraAndPreview();
		mCamera = getCameraInstance();
		mCameraView = view;
		qOpened = (mCamera != null);

		if (qOpened == true) {
			mPreview = new CameraPreview(getActivity().getBaseContext(),
					mCamera, view);
			FrameLayout preview = (FrameLayout) view
					.findViewById(R.id.camera_preview);
			preview.addView(mPreview);
			mPreview.setZOrderOnTop(false);
			mPreview.startCameraPreview();
		}
		return qOpened;
	}

	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(mCurrentCameraId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseCameraAndPreview();
	}

	private void releaseCameraAndPreview() {

		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		if (mPreview != null) {
			mPreview.destroyDrawingCache();
			mPreview.mCamera = null;
		}
	}

	class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

		private SurfaceHolder mHolder;

		private Camera mCamera;

		private List<Camera.Size> mSupportedPreviewSizes;

		private Camera.Size mPreviewSize;

		private List<String> mSupportedFlashModes;
		
		

		public CameraPreview(Context context, Camera camera, View cameraView) {
			super(context);

			mCameraView = cameraView;
			setCamera(camera);

			mHolder = getHolder();
			mHolder.addCallback(this);
			mHolder.setKeepScreenOn(true);
		}

		public void startCameraPreview() {
			try {
				mCamera.setPreviewDisplay(mHolder);
				mCamera.startPreview();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void setCamera(Camera camera) {
			mCamera = camera;
			mSupportedPreviewSizes = mCamera.getParameters()
					.getSupportedPreviewSizes();
			mSupportedFlashModes = mCamera.getParameters()
					.getSupportedFlashModes();

			if (mSupportedFlashModes != null
					&& mSupportedFlashModes
							.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
				Camera.Parameters parameters = mCamera.getParameters();
				parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
				mCamera.setParameters(parameters);
			}

			requestLayout();
		}

		public void surfaceCreated(SurfaceHolder holder) {
			try {
				mCamera.setPreviewDisplay(holder);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			if (mCamera != null) {
				mCamera.stopPreview();
			}
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w,
				int h) {

			if (mHolder.getSurface() == null) {
				return;
			}

			try {
				Camera.Parameters parameters = mCamera.getParameters();

				parameters
						.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

				if (mPreviewSize != null) {
					Camera.Size previewSize = mPreviewSize;
					parameters.setPreviewSize(previewSize.width,
							previewSize.height);
				}
				boolean portrait = isPortrait();
				mCamera.setParameters(parameters);
				mCamera.startPreview();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			final int width = resolveSize(getSuggestedMinimumWidth(),
					widthMeasureSpec);
			final int height = resolveSize(getSuggestedMinimumHeight(),
					heightMeasureSpec);
			setMeasuredDimension(width, height);

			if (mSupportedPreviewSizes != null) {
				mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes,
						width, height);
			}
		}

		@Override
		protected void onLayout(boolean changed, int left, int top, int right,
				int bottom) {
			// Source:
			// http://stackoverflow.com/questions/7942378/android-camera-will-not-work-startpreview-fails
			if (changed) {
				final int width = right - left;
				final int height = bottom - top;

				int previewWidth = width;
				int previewHeight = height;

				if (mPreviewSize != null) {
					Display display = ((WindowManager) getActivity()
							.getSystemService(Context.WINDOW_SERVICE))
							.getDefaultDisplay();

					switch (display.getRotation()) {
					case Surface.ROTATION_0:
						previewWidth = mPreviewSize.height;
						previewHeight = mPreviewSize.width;
						mCamera.setDisplayOrientation(90);
						break;
					case Surface.ROTATION_90:
						previewWidth = mPreviewSize.width;
						previewHeight = mPreviewSize.height;
						break;
					case Surface.ROTATION_180:
						previewWidth = mPreviewSize.height;
						previewHeight = mPreviewSize.width;
						break;
					case Surface.ROTATION_270:
						previewWidth = mPreviewSize.width;
						previewHeight = mPreviewSize.height;
						mCamera.setDisplayOrientation(180);
						break;
					}
				}

				final int scaledChildHeight = previewHeight * width
						/ previewWidth;
				mCameraView
						.layout(0, height - scaledChildHeight, width, height);
			}
		}

		private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes,
				int width, int height) {
			// Source:
			// http://stackoverflow.com/questions/7942378/android-camera-will-not-work-startpreview-fails
			Camera.Size optimalSize = null;

			final double ASPECT_TOLERANCE = 0.1;
			double targetRatio = (double) height / width;

			// Try to find a size match which suits the whole screen minus the
			// menu on the left.
			for (Camera.Size size : sizes) {

				if (size.height != width)
					continue;
				double ratio = (double) size.width / size.height;
				if (ratio <= targetRatio + ASPECT_TOLERANCE
						&& ratio >= targetRatio - ASPECT_TOLERANCE) {
					optimalSize = size;
				}
			}

			// If we cannot find the one that matches the aspect ratio, ignore
			// the requirement.
			if (optimalSize == null) {
				// TODO : Backup in case we don't get a size.
			}

			return optimalSize;
		}
	}

	private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			File pictureFile = getOutputMediaFile();
			if (pictureFile == null) {
				Toast.makeText(getActivity(), "Image retrieval failed.",
						Toast.LENGTH_SHORT).show();
				return;
			}

			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			if (bitmap.getWidth() > bitmap.getHeight()) {
				Matrix matrix = new Matrix();
				matrix.postRotate(270);
				bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
						bitmap.getHeight(), matrix, true);
			}
			Bitmap overlay = BitmapFactory.decodeResource(
					getResources(),
					getActivity().getPreferences(0)
							.getInt(C.SHARED_PREFS_OVERLAY_ITEM,
									R.drawable.instashot01));
			Matrix matrix = new Matrix();
			matrix.preScale(-1.0f, 1.0f);
			Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
					overlay.getWidth(), overlay.getHeight(), matrix, false);

			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				drawOverlay(resizedBitmap, overlay).compress(
						CompressFormat.PNG, 0, bos);
				byte[] bitmapdata = bos.toByteArray();
				fos.write(bitmapdata);
				fos.close();

				safeCameraOpenInView(mCameraView);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};

	private Bitmap drawOverlay(Bitmap bmp1, Bitmap bmp2) {
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(),
				bmp1.getHeight(), bmp1.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp1, new Matrix(), null);
		canvas.drawBitmap(bmp2, new Matrix(), null);
		return bmOverlay;
	}

	@SuppressLint("SimpleDateFormat")
	private File getOutputMediaFile() {

		File ext = Environment.getExternalStorageDirectory();
		String exstPath = ext.getPath();

		File mediaStorageDir = new File(exstPath + "/"
				+ getString(R.string.folder_appname));

		if (!mediaStorageDir.exists()) {
			return null;
		}

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	public boolean isPortrait() {
		return (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
	}

	public void switchCameras() {
		if (mCurrentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
			mCurrentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
		} else {
			mCurrentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
		}
		getFragmentManager()
				.beginTransaction()
				.setCustomAnimations(R.anim.enter_animation,
						R.anim.exit_animation, R.anim.enter_pop_animation,
						R.anim.exit_pop_animation).detach(this).attach(this)
				.commit();

	}

	public void captureImage() {
		mCamera.takePicture(null, null, mPicture);
	}
}
