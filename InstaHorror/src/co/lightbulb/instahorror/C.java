package co.lightbulb.instahorror;

public class C {

	public static final String ARG_SECTION_NUMBER = "section_number";
	public static final String CAMERA_PARAM_ORIENTATION = "orientation";
	public static final String CAMERA_PARAM_LANDSCAPE = "landscape";
	public static final String CAMERA_PARAM_PORTRAIT = "portrait";

	public static final int[] OVERLAYS_IMAGES = { R.drawable.instashot01,
			R.drawable.instashot02, R.drawable.instashot03,
			R.drawable.instashot04, R.drawable.instashot05,
			R.drawable.instashot06, R.drawable.instashot07,
			R.drawable.instashot08 };
	
	public static final String SHARED_PREFS_OVERLAY_ITEM = "OVERLAY_ITEM";

}
