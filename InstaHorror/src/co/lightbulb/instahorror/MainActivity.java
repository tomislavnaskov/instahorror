package co.lightbulb.instahorror;

import java.io.File;

import com.flurry.android.FlurryAdListener;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.FlurryAdType;
import com.flurry.android.FlurryAds;
import com.flurry.android.FlurryAgent;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements
		OnPageChangeListener, OnClickListener, FlurryAdListener {

	private RelativeLayout mTopLayout;
	private ImageButton mCaptureImageButton;
	private ImageButton mSwitchCameraImageButton;
	private ImageButton mGalleryImageButton;
	private CameraFragment mCameraFragment;
	private OverlaySlidesFragmentAdapter mAdapter;
	private ViewPager mPager;
	private PageIndicator mIndicator;
	private FrameLayout mBanner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FlurryAgent.init(this, "XWDGTNDXYN45FHVCJVN5");

		mBanner = (FrameLayout) findViewById(R.id.flurryBanner);
		FlurryAds.setAdListener(this);
		createPictureFolder();
		initActionBar();
		initUi();
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@SuppressLint("InflateParams")
	public void initActionBar() {
		ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar, null);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionBarLayout);
	}

	@Override
	protected void onStart() {
		FlurryAgent.onStartSession(this, "XWDGTNDXYN45FHVCJVN5");
		FlurryAds.fetchAd(this, "InstaHorrorMain-Android", mBanner,
				FlurryAdSize.BANNER_BOTTOM);
		super.onStart();
	}

	@Override
	protected void onStop() {
		FlurryAds.removeAd(this, "InstaHorrorMain-Android", mBanner);
		FlurryAgent.onEndSession(this);
		super.onStop();
	}

	public void initUi() {
		mTopLayout = (RelativeLayout) findViewById(R.id.relativeLayout_top);
		mCaptureImageButton = (ImageButton) findViewById(R.id.imageButton_capture);
		mSwitchCameraImageButton = (ImageButton) findViewById(R.id.imageButton_switch);
		mGalleryImageButton = (ImageButton) findViewById(R.id.imageButton_gallery);
		mCameraFragment = new CameraFragment();
		FragmentTransaction fragmentTransaction;
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(mTopLayout.getId(), mCameraFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
		initOverlays();

	}

	public void initListener() {
		mCaptureImageButton.setOnClickListener(this);
		mSwitchCameraImageButton.setOnClickListener(this);
		mGalleryImageButton.setOnClickListener(this);
		mIndicator.setOnPageChangeListener(this);
	}

	public void initOverlays() {
		mAdapter = new OverlaySlidesFragmentAdapter(getSupportFragmentManager());
		mPager = (OverlayViewPager) findViewById(R.id.viewPagerOverlays);
		mPager.setAdapter(mAdapter);
		mIndicator = (CirclePageIndicator) findViewById(R.id.circleIndicatorOverlays);
		mIndicator.setViewPager(mPager);
		((CirclePageIndicator) mIndicator).setSnap(true);
		mPager.bringToFront();
		initListener();
		getPreferences(0).edit()
				.putInt(C.SHARED_PREFS_OVERLAY_ITEM, C.OVERLAYS_IMAGES[0])
				.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int position) {
		getPreferences(0)
				.edit()
				.putInt(C.SHARED_PREFS_OVERLAY_ITEM,
						C.OVERLAYS_IMAGES[position]).commit();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.imageButton_capture:

			if (mCameraFragment != null) {
				mCameraFragment.captureImage();
			}

			break;

		case R.id.imageButton_switch:
			if (mCameraFragment != null) {
				mCameraFragment.switchCameras();
			}
			break;

		case R.id.imageButton_gallery:
			startActivity(new Intent(this, GalleryActivity.class));
			break;
		default:
			break;
		}

	}

	private void createPictureFolder() {

		String state = Environment.getExternalStorageState();
		if (!Environment.MEDIA_MOUNTED.equals(state)) {
			Toast.makeText(this, getString(R.string.sdcard_not_found),
					Toast.LENGTH_LONG).show();
		} else {
			File exst = Environment.getExternalStorageDirectory();
			String exstPath = exst.getPath();

			File folder = new File(exstPath + "/"
					+ getString(R.string.folder_appname));
			if (!folder.exists()) {
				folder.mkdir();
			}
		}
	}

	@Override
	public void onAdClicked(String arg0) {

	}

	@Override
	public void onAdClosed(String arg0) {

	}

	@Override
	public void onAdOpened(String arg0) {

	}

	@Override
	public void onApplicationExit(String arg0) {

	}

	@Override
	public void onRenderFailed(String arg0) {

	}

	@Override
	public void onRendered(String arg0) {

	}

	@Override
	public void onVideoCompleted(String arg0) {

	}

	@Override
	public boolean shouldDisplayAd(String arg0, FlurryAdType arg1) {
		return true;
	}

	@Override
	public void spaceDidFailToReceiveAd(String arg0) {
	}

	@Override
	public void spaceDidReceiveAd(String arg0) {
		FlurryAds.displayAd(this, "InstaHorrorMain-Android", mBanner);
	}

}
