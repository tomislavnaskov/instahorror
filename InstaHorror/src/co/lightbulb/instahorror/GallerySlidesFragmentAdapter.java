package co.lightbulb.instahorror;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class GallerySlidesFragmentAdapter extends FragmentPagerAdapter {

	private String[] mImagePaths;

	public GallerySlidesFragmentAdapter(FragmentManager fm, String[] imagePaths) {
		super(fm);
		mImagePaths = imagePaths;
	}

	@Override
	public GallerySlidesFragment getItem(int position) {
		return new GallerySlidesFragment(mImagePaths[position]);
	}

	public String getItemPath(int position) {
		return mImagePaths[position];
	}

	@Override
	public int getCount() {
		return mImagePaths.length;
	}
}
