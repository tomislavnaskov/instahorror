package co.lightbulb.instahorror;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class OverlaySlidesFragmentAdapter extends FragmentPagerAdapter implements
		IconPagerAdapter {

	private int[] Images = C.OVERLAYS_IMAGES;

	protected static final int[] ICONS = new int[] {
			R.drawable.indicator_empty, R.drawable.indicator_empty,
			R.drawable.indicator_empty, R.drawable.indicator_empty };

	private int mCount = Images.length;

	public OverlaySlidesFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return new OverlaySlideFragment(Images[position]);
	}

	@Override
	public int getCount() {
		return mCount;
	}

	@Override
	public int getIconResId(int index) {
		return ICONS[index % ICONS.length];
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}