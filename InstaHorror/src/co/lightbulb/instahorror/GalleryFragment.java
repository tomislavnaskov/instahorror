package co.lightbulb.instahorror;

import java.io.File;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

public class GalleryFragment extends Fragment implements OnClickListener {
	private static final float MIN_SCALE = 0.75f;
	private static final float MIN_ALPHA = 0.75f;
	VerticalViewPager verticalViewPager;
	private ImageView mMissingContentImageView;
	private GallerySlidesFragmentAdapter mAdapter;
	private ImageButton mShareImageButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_gallery, container,
				false);
		initUi(view);

		return view;
	}

	private void initUi(View view) {
		mShareImageButton = (ImageButton) view
				.findViewById(R.id.imageButton_share);
		mShareImageButton.setOnClickListener(this);
		verticalViewPager = (VerticalViewPager) view
				.findViewById(R.id.verticalViewpager_gallery);
		mMissingContentImageView = (ImageView) view
				.findViewById(R.id.missingContentImageView);

		initAdapter();

	}

	private void configureVisibility() {
		if (mAdapter != null) {
			if (mAdapter.getCount() > 0) {
				mShareImageButton.setVisibility(View.VISIBLE);
				mShareImageButton.setVisibility(View.VISIBLE);
				verticalViewPager.setVisibility(View.VISIBLE);
				mMissingContentImageView.setVisibility(View.GONE);
			} else {
				mShareImageButton.setVisibility(View.GONE);
				mShareImageButton.setVisibility(View.GONE);
				verticalViewPager.setVisibility(View.GONE);
				mMissingContentImageView.setVisibility(View.VISIBLE);
			}
		}
	}

	private void initAdapter() {

		File mediaStorageDir = new File(
				Environment.getExternalStorageDirectory() + "/"
						+ getActivity().getString(R.string.folder_appname));
		String[] fileNames = mediaStorageDir.list();
		mAdapter = new GallerySlidesFragmentAdapter(getFragmentManager(),
				fileNames);
		verticalViewPager.setAdapter(mAdapter);
		verticalViewPager.setClipToPadding(false);
		verticalViewPager.setPadding(0, 150, 0, 150);
		verticalViewPager.setPageTransformer(true,
				new ViewPager.PageTransformer() {
					@Override
					public void transformPage(View view, float position) {
						int pageWidth = view.getWidth();
						int pageHeight = view.getHeight();

						if (position < -1) {
							view.setAlpha(MIN_ALPHA);

						} else if (position <= 1) {
							float scaleFactor = Math.max(MIN_SCALE,
									1 - Math.abs(position));
							float vertMargin = pageHeight * (1 - scaleFactor)
									/ 2;
							float horzMargin = pageWidth * (1 - scaleFactor)
									/ 2;
							if (position < 0) {
								view.setTranslationY(vertMargin - horzMargin
										/ 2);
							} else {
								view.setTranslationY(-vertMargin + horzMargin
										/ 2);
							}

							view.setScaleX(scaleFactor);
							view.setScaleY(scaleFactor);

							view.setAlpha(MIN_ALPHA + (scaleFactor - MIN_SCALE)
									/ (1 - MIN_SCALE) * (1 - MIN_ALPHA));

						} else {
							view.setAlpha(MIN_ALPHA);
						}
					}
				});

		configureVisibility();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.imageButton_share:
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("image/*");
			String selectedImage = mAdapter.getItemPath(verticalViewPager
					.getCurrentItem());
			File media = new File(Environment.getExternalStorageDirectory()
					+ "/" + getActivity().getString(R.string.folder_appname)
					+ "/" + selectedImage);
			Uri uri = Uri.fromFile(media);
			share.putExtra(Intent.EXTRA_STREAM, uri);
			share.putExtra(Intent.EXTRA_TEXT, getActivity().getResources()
					.getString(R.string.share_text));
			startActivity(Intent.createChooser(share, "Share Image"));

			break;

		default:
			break;
		}

	}
}
