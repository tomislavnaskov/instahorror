package co.lightbulb.instahorror;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class CameraFragment extends android.support.v4.app.Fragment {

	private Camera mCamera;

	private CameraPreview mPreview;

	private static int mCurrentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;

	public CameraFragment() {
		super();
	}

	public static CameraFragment newInstance(int sectionNumber) {
		CameraFragment fragment = new CameraFragment();
		Bundle args = new Bundle();
		args.putInt(C.ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_native_camera,
				container, false);

		boolean opened = safeCameraOpenInView(view);

		if (opened == false) {
			Log.d("CameraGuide", "Error, Camera failed to open");
			return view;
		}

		return view;
	}

	protected boolean safeCameraOpenInView(View view) {
		boolean qOpened = false;
		releaseCameraAndPreview();
		mCamera = getCameraInstance();
		qOpened = (mCamera != null);

		if (qOpened == true) {
			mPreview = new CameraPreview(getActivity().getBaseContext(),
					mCamera);
			FrameLayout preview = (FrameLayout) view
					.findViewById(R.id.camera_preview);
			preview.addView(mPreview);
		}
		return qOpened;
	}

	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(mCurrentCameraId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c;
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseCameraAndPreview();
	}

	private void releaseCameraAndPreview() {

		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		if (mPreview != null) {
			mPreview.destroyDrawingCache();
			mPreview.mCamera = null;
		}
	}

	public class CameraPreview extends SurfaceView implements
			SurfaceHolder.Callback {

		private SurfaceHolder mHolder;
		private Camera mCamera;
		protected Camera.Size mPreviewSize;
		protected Camera.Size mPictureSize;

		public CameraPreview(Context context, Camera camera) {
			super(context);
			mCamera = camera;
			mHolder = getHolder();
			mHolder.addCallback(this);
		}

		public void surfaceCreated(SurfaceHolder holder) {
			try {
				mCamera.setPreviewDisplay(holder);
				setCameraDisplayOrientation(getActivity(), mCurrentCameraId,
						mCamera);
				mCamera.startPreview();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w,
				int h) {

			if (mHolder.getSurface() == null) {
				return;
			}

			try {
				mCamera.stopPreview();
				mCamera.setPreviewDisplay(mHolder);
				mCamera.startPreview();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			Camera.Parameters params = mCamera.getParameters();
			if (mCurrentCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				params.setPictureSize(params.getPreviewSize().width,
						params.getPreviewSize().height);
			} else {

				// TODO optimize back camera quality
				Size optimalPreviewSize = Utils.getBestPreviewSize(
						params.getPreviewSize().width,
						params.getPreviewSize().height, mCamera);
				params.setPreviewSize(optimalPreviewSize.width,
						optimalPreviewSize.height);
				Size optimalPictureSize = Utils.getOptimalPictureSize(
						params.getPreviewSize().width,
						params.getPreviewSize().height, params);
				params.setPictureSize(optimalPictureSize.width,
						optimalPictureSize.height);
				params.setFocusMode(Parameters.FOCUS_MODE_AUTO);

			}
			mCamera.setParameters(params);
			setMeasuredDimension(params.getPreviewSize().height,
					params.getPreviewSize().width);

		}
	}

	private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {

			File pictureFile = Utils.getOutputMediaFile(getActivity());
			if (pictureFile == null) {
				Toast.makeText(
						getActivity(),
						getActivity().getString(
								R.string.error_image_retrival_failed),
						Toast.LENGTH_SHORT).show();
				return;
			}
			LongOperation longOperation = new LongOperation();
			longOperation.setLongOperationParameters(data, getActivity(),
					pictureFile, mCurrentCameraId);
			longOperation.execute("");
			reopenCameraAfterPhotoTaken();
		}
	};

	public void switchCameras() {
		if (mCurrentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
			mCurrentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
		} else {
			mCurrentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
		}
		getFragmentManager()
				.beginTransaction()
				.setCustomAnimations(R.anim.enter_animation,
						R.anim.exit_animation, R.anim.enter_pop_animation,
						R.anim.exit_pop_animation).detach(this).attach(this)
				.commit();
	}

	public void reopenCameraAfterPhotoTaken() {
		getFragmentManager()
				.beginTransaction()
				.setCustomAnimations(R.anim.enter_animation,
						R.anim.exit_animation, R.anim.enter_pop_animation,
						R.anim.exit_pop_animation).detach(this).attach(this)
				.commit();
	}

	public void captureImage() {
		mCamera.takePicture(null, null, mPicture);
	}

	private void setCameraDisplayOrientation(Activity activity, int cameraId,
			android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;
		} else {
			result = (info.orientation - degrees + 360) % 360;
		}
		camera.setDisplayOrientation(result);
	}

	private class LongOperation extends AsyncTask<String, Void, String> {

		byte[] data;
		Context context;
		File pictureFile;
		int cameraId;

		public void setLongOperationParameters(byte[] data, Context context,
				File pictureFile, int cameraId) {
			this.data = data;
			this.context = context;
			this.pictureFile = pictureFile;
			this.cameraId = cameraId;

		}

		@Override
		protected String doInBackground(String... params) {
			Utils.modifyAndSavePicture(data, context, cameraId, pictureFile);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			Toast.makeText(context,
					context.getString(R.string.success_saved_picture_galery),
					Toast.LENGTH_LONG).show();
			super.onPostExecute(result);
		}

	}
}
