package co.lightbulb.instahorror;

import com.flurry.android.FlurryAdListener;
import com.flurry.android.FlurryAdSize;
import com.flurry.android.FlurryAdType;
import com.flurry.android.FlurryAds;
import com.flurry.android.FlurryAgent;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class GalleryActivity extends ActionBarActivity implements
		FlurryAdListener {

	private RelativeLayout mParrentLayout;
	private GalleryFragment mGalleryFragment;
	private FrameLayout mBanner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);
		FlurryAgent.init(this, "XWDGTNDXYN45FHVCJVN5");
		mBanner = (FrameLayout) findViewById(R.id.flurryBanner);
		FlurryAds.setAdListener(this);
		initUi();

	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@SuppressLint("InflateParams")
	public void initActionBar() {
		ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar, null);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionBarLayout);
	}

	@Override
	protected void onStart() {
		FlurryAgent.onStartSession(this, "XWDGTNDXYN45FHVCJVN5");
		FlurryAds.fetchAd(this, "InstaHorrorGallery-Android", mBanner,
				FlurryAdSize.BANNER_BOTTOM);
		super.onStart();
	}

	@Override
	protected void onStop() {
		FlurryAds.removeAd(this, "InstaHorrorGallery-Android", mBanner);
		FlurryAgent.onEndSession(this);
		super.onStop();
	}

	private void initUi() {
		mParrentLayout = (RelativeLayout) findViewById(R.id.relativeLayout_parrent_gallery);

		mGalleryFragment = new GalleryFragment();
		FragmentTransaction fragmentTransaction;
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(mParrentLayout.getId(), mGalleryFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	@Override
	public void onAdClicked(String arg0) {

	}

	@Override
	public void onAdClosed(String arg0) {

	}

	@Override
	public void onAdOpened(String arg0) {

	}

	@Override
	public void onApplicationExit(String arg0) {

	}

	@Override
	public void onRenderFailed(String arg0) {

	}

	@Override
	public void onRendered(String arg0) {

	}

	@Override
	public void onVideoCompleted(String arg0) {

	}

	@Override
	public boolean shouldDisplayAd(String arg0, FlurryAdType arg1) {
		return true;
	}

	@Override
	public void spaceDidFailToReceiveAd(String arg0) {

	}

	@Override
	public void spaceDidReceiveAd(String arg0) {
		FlurryAds.displayAd(this, "InstaHorrorGallery-Android", mBanner);
	}
}
