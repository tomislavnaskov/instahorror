package co.lightbulb.instahorror;

public interface CustomButtonClickListener {

	public void onCaptureButtonClick();

	public void onSwitchCameraButtonClick();

}
