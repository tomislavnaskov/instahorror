package co.lightbulb.instahorror;

import java.io.File;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SlidingPaneLayout.LayoutParams;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public final class GallerySlidesFragment extends Fragment {
	String mImagePath;

	public GallerySlidesFragment(String imagePath) {
		mImagePath = imagePath;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.gallery_item_layout, container,
				false);

		final ImageView imageView = (ImageView) view
				.findViewById(R.id.imageView_gallery_item);
		File imageFile = new File(Environment.getExternalStorageDirectory()
				+ "/" + getActivity().getString(R.string.folder_appname) + "/"
				+ mImagePath);
		Picasso.with(getActivity()).load(imageFile).into(imageView);

		return view;
	}

}
